<?php

declare(strict_types=1);

namespace Drupal\entity_extra_field;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Define extra field type plugin interface.
 */
interface ExtraFieldTypePluginInterface extends PluginFormInterface, ContainerFactoryPluginInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Display the extra field plugin label.
   *
   * @return string
   *   Return the extra field plugin label.
   */
  public function label(): string;

  /**
   * Build the render array of the extra field type contents.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity type the extra field is being attached too.
   * @param \Drupal\Core\Entity\Display\EntityDisplayInterface $display
   *   The entity displaying the extra field is a part of.
   *
   * @return array
   *   The extra field renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function build(EntityInterface $entity, EntityDisplayInterface $display): array;

  /**
   *  Determine if the extra field type plugin is applicable.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   *
   * @return bool
   *    Return TRUE if the plugin restrictions are met; otherwise FALSE.
   */
  public static function isApplicable(ModuleHandlerInterface $module_handler): bool;
}
