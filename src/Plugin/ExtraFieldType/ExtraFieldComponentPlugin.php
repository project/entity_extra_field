<?php

declare(strict_types=1);

namespace Drupal\entity_extra_field\Plugin\ExtraFieldType;

use Drupal\Core\Utility\Error;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Utility\Token;
use Drupal\entity_extra_field\ExtraFieldTypePluginBase;
use Drupal\sdc\ComponentPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the extra field component plugin.
 *
 * @ExtraFieldType(
 *   id = "component",
 *   label = @Translation("Component")
 * )
 */
class ExtraFieldComponentPlugin extends ExtraFieldTypePluginBase {

  /**
   * @var \Drupal\sdc\ComponentPluginManager
   */
  protected ComponentPluginManager $componentPluginManager;

  /**
   * Define the class constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\sdc\ComponentPluginManager $component_plugin_manager
   *   The component plugin manager service.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    Token $token,
    ModuleHandlerInterface $module_handler,
    RouteMatchInterface $current_route_match,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    ComponentPluginManager $component_plugin_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $token,
      $module_handler,
      $current_route_match,
      $entity_type_manager,
      $entity_field_manager,
    );
    $this->componentPluginManager = $component_plugin_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('token'),
      $container->get('module_handler'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.sdc')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'component' => NULL,
      'settings' => [
        'mapping' => [
          'slots' => [],
          'props' => [],
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();
    $component_data = $this->getComponentData();

    $component = $this->getPluginFormStateValue(
      'component',
      $form_state,
      $configuration['component'] ?? NULL
    );

    $form['component'] = [
      '#type' => 'select',
      '#title' => $this->t('Component'),
      '#options' => $component_data['options'],
      '#default_value' => $component,
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select -'),
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
      ] + $this->extraFieldPluginAjax(),
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Component settings'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $settings = $configuration['settings'] ?? [];

    if ($component_mapping = $component_data['mappings'][$component]) {
      foreach ($component_mapping as $type => $mapping) {
        if (count($mapping) === 0) {
          continue;
        }
        if (!isset($form['mapping'][$type])) {
          $form['settings']['mapping'][$type] = [
            '#type' => 'details',
            '#title' => $this->t(Unicode::ucwords($type)),
          ];
        }
        $mapping_settings = $settings['mapping'][$type] ?? [];

        foreach ($mapping as $name => $info) {
          if (!isset($info['type'])) {
            continue;
          }
          $form['settings']['mapping'][$type][$name] = [
            '#type' => 'textfield',
            '#title' => $info['title'],
            '#required' => $info['required'],
            '#default_value' => $mapping_settings[$name] ?? NULL
          ];
        }
      }
      $this->addTokenReplacements($form['settings']);
    } else {
      $form['settings']['no_mapping']['#markup'] = $this->t(
        'There are no props/slots for the selected component'
      );
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build(
    EntityInterface $entity,
    EntityDisplayInterface $display
  ): array {
    $build = [];
    $configuration = $this->getConfiguration();

    if (isset($configuration['component'])) {
      $build = [
        '#type' => 'component',
        '#component' => $configuration['component'],
      ];

      if (
        ($component_mapping = $configuration['settings']['mapping'])
        && $entity instanceof ContentEntityInterface
      ) {
        foreach ($component_mapping as $type => $values) {
          if (count($values) === 0) {
            continue;
          }
          $build["#$type"] = array_map(function ($value) use($entity) {
            return $this->processEntityToken($value, $entity);
          }, array_filter($values));
        }
      }
    }

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  public static function isApplicable(
    ModuleHandlerInterface $module_handler
  ): bool {
    return $module_handler->moduleExists('sdc');
  }

  /**
   * Get the component normalized data.
   */
  protected function getComponentData(): array {
    $data = [
      'options' => [],
      'mappings' => []
    ];
    $definitions = $this->componentPluginManager->getDefinitions();

    foreach ($definitions as $plugin_id => $definition) {
      if (!isset($definition['name'])) {
        continue;
      }
      $data['options'][$plugin_id] = $definition['name'];

      if (
        isset($definition['slots'])
        && ($slots = $definition['slots'])
        && $slots_info = $this->normalizeComponentSlotInfo($slots)
      ) {
        $data['mappings'][$plugin_id]['slots'] = $slots_info;
      }

      if (
        isset($definition['props'])
        && ($props = $definition['props'])
        && $props_info = $this->normalizeComponentPropInfo($props)
      ) {
        $data['mappings'][$plugin_id]['props'] = $props_info;
      }
    }
    asort($data['options']);

    return $data;
  }

  /**
   * Normalize component slot information.
   *
   * @param array $slots
   *   An array of component slot definitions.
   *
   * @return array
   *    An array of the normalize component slots:
   *      - type: The component slot type.
   *      - title: The component slot title.
   *      - required: The component slot required flag.
   */
  protected function normalizeComponentSlotInfo(
    array $slots,
  ): array {
    $info = [];

    foreach ($slots as $name => $slot_info) {
      $info[$name] = [
        'type' => 'string',
        'title' => $slot_info['title'] ?? $this->formatTitle($name),
        'required' => $slot_info['required'] ?? FALSE,
      ];
    }

    return $info;
  }

  /**
   * Normalize the component prop information.
   *
   * @param array $props
   *   An array of the component prop definitions.
   *
   * @return array
   *   An array of the normalize component props:
   *     - type: The component props type.
   *     - title: The component props title.
   *     - required: The component props required flag.
   */
  protected function normalizeComponentPropInfo(
    array $props,
  ): array {
    $info = [];

    if (isset($props['properties'])) {
      $required = $props['required'] ?? [];

      foreach ($props['properties'] as $name => $prop_info) {
        $info[$name] = [
          'type' => $prop_info['type'] ?? 'string',
          'title' => $prop_info['title'] ?? $this->formatTitle($name),
          'required' => in_array($name, array_values($required), TRUE),
        ];
      }
    }

    return $info;
  }

  /**
   * Add token replacements.
   *
   * @param array $form
   *   An array of the form.
   */
  protected function addTokenReplacements(array &$form): void {
    try {
      if ($this->moduleHandler->moduleExists('token')) {
        $bundleType = $this->getTargetEntityTypeBundle();

        $form['token_replacements'] = [
          '#theme' => 'token_tree_link',
          '#token_types' => $this->getEntityTokenTypes(
            $this->getTargetEntityTypeDefinition(),
            (string) ($bundleType
              ? $bundleType->id()
              : $this->getTargetEntityTypeId()
            )
          ),
        ];
      }
    } catch (\Exception $exception) {
      Error::logException(
        \Drupal::logger('entity_extra_field'),
        $exception
      );
    }
  }

  /**
   * Format the component title.
   *
   * @param string $title
   *   The value to use as the title.
   *
   * @return string
   *   The formatted title.
   */
  protected function formatTitle(
    string $title
  ): string {
    return Unicode::ucwords(str_replace(
      '_',
      ' ',
      $title
    ));
  }
}
