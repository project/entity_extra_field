<?php

declare(strict_types=1);

namespace Drupal\entity_extra_field\Plugin\ExtraFieldType;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_extra_field\ExtraFieldTypePluginBase;

/**
 * Define extra field twig plugin.
 *
 * @ExtraFieldType(
 *   id = "twig",
 *   label = @Translation("Twig")
 * )
 */
class ExtraFieldTwigPlugin extends ExtraFieldTypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'twig_template' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['twig_template'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Twig Template'),
      '#required' => TRUE,
      '#description' => $this->t('Provide the Twig code to render.<br> These are the context variables you can use in the template: @context.<br>The "entity" variable is special in that it represents the entity the extra field resides on. If you want to access the entity title, for example, you can use <code>{{ entity.title.value }}</code>.<br>Furthermore, you can alter the twig context through "hook_entity_extra_field_twig_context_alter." For an example implementation, check out "entity_extra_field.api.php".', ['@context' => implode(', ', array_keys($this->getContext()))]),
      '#default_value' => $this->getConfiguration()['twig_template'],
      '#rows' => 20,
    ];

    return $form;
  }

  /**
   * Validation callback for a Template element.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) : void {
    $build = [
      '#type' => 'inline_template',
      '#template' => $form_state->getValue('twig_template'),
      // We are not passing all the context, but that is fine, as using
      // unavailable context variables won't result in an exception:
      '#context' => $this->getContext(),
    ];
    try {
      \Drupal::service('renderer')->renderPlain($build);
    }
    catch (\Exception $exception) {
      $form_state->setError($form, $this->t(
        'Template error: @error',
        ['@error' => $exception->getMessage()])
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(EntityInterface $entity, EntityDisplayInterface $display): array {
    $build = [];

    if ($entity instanceof ContentEntityInterface) {
      // Get te twig template output:
      $twigTemplateOutput = $this->getConfiguration()['twig_template'];

      // Get the global twig context:
      $context = $this->getContext($entity);

      // Invoke module alter hook to modify the context:
      $this->moduleHandler->alter('entity_extra_field_twig_context', $context, $entity, $display);

      $build = [
        '#type' => 'inline_template',
        '#template' => $twigTemplateOutput,
        '#context' => $context,
      ];

    }
    return $build;
  }

  /**
   * Provides context variables for the twig template.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
   *   The entity object.
   *
   * @return array
   *   The context variables.
   */
  protected function getContext(?ContentEntityInterface $entity = NULL): array {
    $context = [];

    // Add the global context variables:
    $theme = \Drupal::theme()->getActiveTheme();
    $context['theme'] = $theme->getName();
    $context['theme_directory'] = $theme->getPath();

    $context['base_path'] = base_path();
    $context['front_page'] = Url::fromRoute('<front>');
    $context['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
    $context['language'] = \Drupal::languageManager()->getCurrentLanguage();

    $user = \Drupal::currentUser();
    $context['is_admin'] = $user->hasPermission('access administration pages');
    $context['logged_in'] = $user->isAuthenticated();

    // Add the entity context, even if it is NULL, so it is displayed in the
    // twig_template description:
    $context['entity'] = $entity;

    return $context;
  }

}
