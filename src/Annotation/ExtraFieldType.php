<?php

declare(strict_types=1);

namespace Drupal\entity_extra_field\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define extra field type plugin annotation.
 *
 * @Annotation
 */
class ExtraFieldType extends Plugin {

  /**
   * @var string
   */
  public string $id;

  /**
   * @var string
   */
  public string $label;

}
