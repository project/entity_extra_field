<?php

/**
 * @file
 * Describe hooks provided by the Entity Extra Field module.
 */

use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the "twig" extra field context for rendering the inline twig template.
 *
 * @param array $context
 *   The twig context to alter.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity the extra field is attached to.
 * @param \Drupal\Core\Entity\Display\EntityDisplayInterface $display
 *   The entity display object on which the extra field is rendered.
 */
function hook_entity_extra_field_twig_context_alter(&$context, EntityInterface $entity, EntityDisplayInterface $display) {
  // Add a new variable to the twig context, when the entity is an 'article':
  if ($entity->bundle() === 'article') {
    $context['new_variable'] = 'New variable value';
  }
}
