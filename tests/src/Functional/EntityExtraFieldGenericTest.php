<?php

namespace Drupal\Tests\entity_extra_field\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for entity_extra_field.
 *
 * @group entity_extra_field
 */
class EntityExtraFieldGenericTest extends GenericModuleTestBase {

  /**
   * {@inheritDoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. Just overwrite this method, so we don't have to
    // implement hook_help().
  }

}
